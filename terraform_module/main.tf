resource "helm_release" "directus" {
  chart           = "directus"
  repository      = "https://gitlab.com/api/v4/projects/30400986/packages/helm/stable"
  name            = var.chart_name
  namespace       = var.namespace
  version         = var.chart_version
  force_update    = var.helm_force_update
  recreate_pods   = var.helm_recreate_pods
  cleanup_on_fail = var.helm_cleanup_on_fail
  max_history     = var.helm_max_history

  values = concat([
    file("${path.module}/directus.yaml"),
  ], var.values)

  dynamic "set_sensitive" {
    for_each = var.directus_key == null ? [] : [var.directus_key]
    content {
      name  = "key"
      value = var.directus_key
    }
  }

  dynamic "set_sensitive" {
    for_each = var.directus_secret == null ? [] : [var.directus_secret]
    content {
      name  = "secret"
      value = var.directus_secret
    }
  }

  dynamic "set" {
    for_each = var.image_repository == null ? [] : [var.image_repository]
    content {
      name  = "image.repository"
      value = var.image_repository
    }
  }

  dynamic "set" {
    for_each = var.image_tag == null ? [] : [var.image_tag]
    content {
      name  = "image.tag"
      value = var.image_tag
    }
  }

  dynamic "set" {
    for_each = var.directus_public_url == null ? [] : [var.directus_public_url]
    content {
      name  = "publicUrl"
      value = var.directus_public_url
    }
  }


  dynamic "set" {
    for_each = var.directus_postgresql_deploy == null ? [] : [var.directus_postgresql_deploy]
    content {
      name  = "postgresql.deploy"
      value = var.directus_postgresql_deploy
    }
  }
  dynamic "set" {
    for_each = var.directus_postgresql_username == null ? [] : [var.directus_postgresql_username]
    content {
      name  = "postgresql.postgresqlUsername"
      value = var.directus_postgresql_username
    }
  }
  dynamic "set_sensitive" {
    for_each = var.directus_postgresql_password == null ? [] : [var.directus_postgresql_password]
    content {
      name  = "postgresql.postgresqlPassword"
      value = var.directus_postgresql_password
    }
  }
  dynamic "set" {
    for_each = var.directus_postgresql_database == null ? [] : [var.directus_postgresql_database]
    content {
      name  = "postgresql.postgresqlDatabase"
      value = var.directus_postgresql_database
    }
  }
  dynamic "set" {
    for_each = var.directus_postgresql_host == null ? [] : [var.directus_postgresql_host]
    content {
      name  = "postgresql.postgresqlHost"
      value = var.directus_postgresql_host
    }
  }
  dynamic "set" {
    for_each = var.directus_postgresql_port == null ? [] : [var.directus_postgresql_port]
    content {
      name  = "postgresql.service.port"
      value = var.directus_postgresql_port
    }
  }
  dynamic "set" {
    for_each = var.directus_postgresql_ssl == null ? [] : [var.directus_postgresql_ssl]
    content {
      name  = "postgresql.postgresqlSSL"
      value = var.directus_postgresql_ssl
    }
  }
  dynamic "set" {
    for_each = var.directus_postgresql_ssl_reject_unauthorized == null ? [] : [var.directus_postgresql_ssl_reject_unauthorized]
    content {
      name  = "postgresql.postgresqlSSLRejectUnauthorized"
      value = var.directus_postgresql_ssl_reject_unauthorized
    }
  }

  dynamic "set" {
    for_each = var.directus_email_from == null ? [] : [var.directus_email_from]
    content {
      name  = "email.from"
      value = var.directus_email_from
    }
  }
  dynamic "set" {
    for_each = var.directus_email_smtp_host == null ? [] : [var.directus_email_smtp_host]
    content {
      name  = "email.smtp.host"
      value = var.directus_email_smtp_host
    }
  }
  dynamic "set" {
    for_each = var.directus_email_smtp_user == null ? [] : [var.directus_email_smtp_user]
    content {
      name  = "email.smtp.user"
      value = var.directus_email_smtp_user
    }
  }
  dynamic "set" {
    for_each = var.directus_email_smtp_password == null ? [] : [var.directus_email_smtp_password]
    content {
      name  = "email.smtp.password"
      value = var.directus_email_smtp_password
    }
  }
  dynamic "set" {
    for_each = var.directus_email_smtp_port == null ? [] : [var.directus_email_smtp_port]
    content {
      name  = "email.smtp.port"
      value = var.directus_email_smtp_port
    }
  }
  dynamic "set" {
    for_each = var.directus_email_smtp_secure == null ? [] : [var.directus_email_smtp_secure]
    content {
      name  = "email.smtp.secure"
      value = var.directus_email_smtp_secure
    }
  }

  dynamic "set" {
    for_each = var.directus_storage_scaleway_bucket == null ? [] : [var.directus_storage_scaleway_bucket]
    content {
      name  = "storage.scaleway.bucket"
      value = var.directus_storage_scaleway_bucket
    }
  }
  dynamic "set" {
    for_each = var.directus_storage_scaleway_endpoint == null ? [] : [var.directus_storage_scaleway_endpoint]
    content {
      name  = "storage.scaleway.endpoint"
      value = var.directus_storage_scaleway_endpoint
    }
  }
  dynamic "set" {
    for_each = var.directus_storage_scaleway_key == null ? [] : [var.directus_storage_scaleway_key]
    content {
      name  = "storage.scaleway.key"
      value = var.directus_storage_scaleway_key
    }
  }
  dynamic "set" {
    for_each = var.directus_storage_scaleway_region == null ? [] : [var.directus_storage_scaleway_region]
    content {
      name  = "storage.scaleway.region"
      value = var.directus_storage_scaleway_region
    }
  }
  dynamic "set" {
    for_each = var.directus_storage_scaleway_secret == null ? [] : [var.directus_storage_scaleway_secret]
    content {
      name  = "storage.scaleway.secret"
      value = var.directus_storage_scaleway_secret
    }
  }

  dynamic "set" {
    for_each = var.limits_cpu == null ? [] : [var.limits_cpu]
    content {
      name  = "resources.limits.cpu"
      value = var.limits_cpu
    }
  }

  dynamic "set" {
    for_each = var.limits_memory == null ? [] : [var.limits_memory]
    content {
      name  = "resources.limits.memory"
      value = var.limits_memory
    }
  }

  dynamic "set" {
    for_each = var.requests_cpu == null ? [] : [var.requests_cpu]
    content {
      name  = "resources.requests.cpu"
      value = var.requests_cpu
    }
  }

  dynamic "set" {
    for_each = var.requests_memory == null ? [] : [var.requests_memory]
    content {
      name  = "resources.requests.memory"
      value = var.requests_memory
    }
  }

  dynamic "set" {
    for_each = var.ingress_host == null ? [] : [var.ingress_host]
    content {
      name  = "ingress.host"
      value = var.ingress_host
    }
  }

  dynamic "set" {
    for_each = var.directus_cors_enabled == null ? [] : [var.directus_cors_enabled]
    content {
      name  = "cors.enabled"
      value = var.directus_cors_enabled
    }
  }

  dynamic "set" {
    for_each = var.directus_cors_origin == null ? [] : [var.directus_cors_origin]
    content {
      name  = "cors.origin"
      value = var.directus_cors_origin
    }
  }
}
