variable "directus_key" {
  type    = string
  default = null
}

variable "directus_secret" {
  type    = string
  default = null
}

variable "directus_public_url" {
  type    = string
  default = null
}

variable "directus_postgresql_deploy" {
  type    = bool
  default = false
}

variable "directus_postgresql_username" {
  type    = string
  default = "postgres"
}

variable "directus_postgresql_password" {
  type    = string
  default = ""
}

variable "directus_postgresql_ssl" {
  type    = bool
  default = null
}

variable "directus_postgresql_ssl_reject_unauthorized" {
  type    = bool
  default = null
}

variable "directus_postgresql_database" {
  type    = string
  default = ""
}

variable "directus_postgresql_host" {
  type    = string
  default = null
}

variable "directus_postgresql_port" {
  type    = string
  default = 5432
}

variable "directus_email_from" {
  type    = string
  default = null
}
variable "directus_email_smtp_host" {
  type    = string
  default = null
}
variable "directus_email_smtp_user" {
  type    = string
  default = null
}
variable "directus_email_smtp_password" {
  type    = string
  default = null
}
variable "directus_email_smtp_port" {
  type    = string
  default = null
}
variable "directus_email_smtp_secure" {
  type    = string
  default = null
}

variable "directus_storage_scaleway_bucket" {
  type    = string
  default = null
}

variable "directus_storage_scaleway_endpoint" {
  type    = string
  default = null
}

variable "directus_storage_scaleway_key" {
  type    = string
  default = null
}

variable "directus_storage_scaleway_region" {
  type    = string
  default = null
}

variable "directus_storage_scaleway_secret" {
  type    = string
  default = null
}

variable "directus_cors_enabled" {
  type    = bool
  default = null
}

variable "directus_cors_origin" {
  type    = bool
  default = null
}
