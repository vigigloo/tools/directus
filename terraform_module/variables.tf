variable "namespace" {
  type = string
}

variable "chart_name" {
  type = string
}

variable "chart_version" {
  type    = string
  default = "0.4.0"
}

variable "values" {
  type    = list(string)
  default = []
}

variable "image_repository" {
  type    = string
  default = "directus/directus"
}

variable "image_tag" {
  type    = string
  default = null
}

variable "helm_force_update" {
  type    = bool
  default = false
}

variable "helm_recreate_pods" {
  type    = bool
  default = false
}

variable "helm_cleanup_on_fail" {
  type    = bool
  default = false
}

variable "helm_max_history" {
  type    = number
  default = 0
}

variable "limits_cpu" {
  type    = string
  default = "1000m"
}

variable "limits_memory" {
  type    = string
  default = "1024Mi"
}

variable "requests_cpu" {
  type    = string
  default = "500m"
}

variable "requests_memory" {
  type    = string
  default = "512Mi"
}
