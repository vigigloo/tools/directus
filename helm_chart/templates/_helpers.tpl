{{- define "common.commonEnv" -}}
- name: PUBLIC_URL
  value: {{ .Values.publicUrl | quote }}
- name: KEY
  value: {{ .Values.key | default (include "common.fullname" .) | quote }}
- name: SECRET
  value: {{ .Values.secret | default (randAlphaNum 32) | quote }}
- name: DB_CLIENT
  value: pg
{{- with .Values.postgresql }}
- name: DB_PORT
{{- if .deploy }}
  value: 5432
{{- else }}
  value: {{ .service.port | default "5432" | quote }}
{{- end }}
- name: DB_DATABASE
  value: {{ .postgresqlDatabase | quote }}
{{- if ne .postgresqlSSL nil }}
- name: DB_SSL
  value: {{ .postgresqlSSL | quote }}
{{- end }}
{{- if ne .postgresqlSSLRejectUnauthorized nil }}
- name: DB_SSL__REJECT_UNAUTHORIZED
  value: {{ .postgresqlSSLRejectUnauthorized | quote }}
{{- end }}
- name: DB_HOST
{{- if .deploy }}
  value: {{ printf "%s-%s" $.Release.Name "postgresql" | trunc 63 | trimSuffix "-" }}
{{- else }}
  value: {{ .postgresqlHost | quote }}
{{- end }}
- name: DB_USER
  value: {{ .postgresqlUsername | quote }}
- name: DB_PASSWORD
{{- if .deploy }}
  valueFrom:
    secretKeyRef:
      name: {{ printf "%s-%s" $.Release.Name "postgresql" | trunc 63 | trimSuffix "-" }}
      key: postgresql-password
{{- else }}
  value: {{ .postgresqlPassword | quote }}
{{- end }}
{{- end }}{{/* end with */}}

- name: CORS_ENABLED
  value: {{ .Values.cors.enabled | quote }}
- name: CORS_ORIGIN
  value: {{ .Values.cors.origin | quote }}

{{- with .Values.email }}
{{- if . }}
{{- if .from }}
- name: EMAIL_FROM
  value: {{ .from | quote }}
{{- end }}
{{- with .smtp }}
{{- if . }}
- name: EMAIL_TRANSPORT
  value: "smtp"
- name: EMAIL_SMTP_HOST
  value: {{ .host | quote }}
- name: EMAIL_SMTP_PASSWORD
  value: {{ .password | quote }}
- name: EMAIL_SMTP_PORT
  value: {{ .port | quote }}
- name: EMAIL_SMTP_SECURE
  value: {{ .secure | quote }}
- name: EMAIL_SMTP_USER
  value: {{ .user | quote }}
{{- end }}{{/*end if smtp*/}}
{{- end}}{{/* end with smtp */}}
{{- end }}{{/*end if email*/}}
{{- end }}{{/* end with email */}}

{{- with .Values.storage }}
{{- with .scaleway }}
{{- if . }}
- name: STORAGE_LOCATIONS
  value: "scaleway"
- name: STORAGE_SCALEWAY_DRIVER
  value: "s3"
- name: STORAGE_SCALEWAY_BUCKET
  value: {{ .bucket | quote }}
- name: STORAGE_SCALEWAY_ENDPOINT
  value: {{ .endpoint | quote }}
- name: STORAGE_SCALEWAY_KEY
  value: {{ .key | quote }}
- name: STORAGE_SCALEWAY_REGION
  value: {{ .region | quote }}
- name: STORAGE_SCALEWAY_SECRET
  value: {{ .secret | quote }}
{{- end }}{{/*end if scaleway*/}}
{{- end}}{{/* end with scaleway */}}
{{- end }}{{/* end with emstorage */}}

{{/* additional env vars */}}
{{- include "common.env.transformDict" .Values.envVars -}}
{{- end }}{{/* end define */}}
